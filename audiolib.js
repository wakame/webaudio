function osciSin(freq, t) {
  // TODO 48000 hardcoded, use audioCtx.sampleRate instead.
  let result = Math.sin(2 * Math.PI * freq * t / 48000);
  return result;
}

export function osciSinWrap(node, env, evaluate) {
  let freq = evaluate(node[1], env);
  let t = evaluate(node[2], env);
  return osciSin(freq, t);
}

export function osciSawWrap(node, env, evaluate) {
  let freq = evaluate(node[1], env);
  let t = evaluate(node[2], env);

  // sawtooth goes from 1 to -1.

  // TODO 48000 hardcoded, use audioCtx.sampleRate instead.
  let result = freq * t / 48000;

  result = result - Math.floor(result);
  
  result = 1 - 2*result;
  
  return result;  
}

export function vcaWrap(node, env, evaluate) {
  let value = evaluate(node[1], env);
  let volume = evaluate(node[2], env);

  volume = Math.abs(volume);

  return value*volume;
}

export function envelope(node, env, evaluate) {
  let input = evaluate(node[1], env);

  let attack = evaluate(node[2], env);
  let sustain = evaluate(node[3], env);
  let release = evaluate(node[4], env);
  let decay = evaluate(node[5], env);
  
}

export function sequence(node, env, evaluate) {
  // TODO Another hack because we don't support float values in trip yet...
  let duration = evaluate(node[1], env) / 1000;
  let t = evaluate(node[2], env);

  // TODO 48000 hardcoded, use audioCtx.sampleRate instead.
  // +3 since we start at index 3.
  let index = Math.floor(t/(duration*48000))+3;

  if(index>=node.length) {
    return 0;
  }

  let input = node[index];
  return evaluate(input, env);
}


export function range(node, env, evaluate) {
  // TODO Another hack because we don't support float values in trip yet...
  let start = evaluate(node[2], env)/1000;
  let stop = evaluate(node[3], env)/1000;
  let t = evaluate(node[4], env);

  // TODO 48000 hardcoded, use audioCtx.sampleRate instead.
  if(t<start*48000) {
    return 0.0;
  }

  // TODO 48000 hardcoded, use audioCtx.sampleRate instead.
  if(t>stop*48000) {
    return 0.0;
  }

  return evaluate(node[1], env);
}

export function decay(node, env, evaluate) {
  let value = evaluate(node[1], env);
  let end = evaluate(node[2], env)/1000;
  let t = evaluate(node[3], env);

  // TODO 48000 hardcoded, use audioCtx.sampleRate instead.
  // Linear falloff for now.
  let falloff = -1 / (end*48000);

  let volume = Math.max(1 + falloff*t, 0);

  return value*volume;
}


export function addAudio(env) {
  env["sin"] = osciSinWrap;
  env["saw"] = osciSawWrap;
  env["vca"] = vcaWrap;
  env["sequence"] = sequence;
  env["range"] = range;
  env["decay"] = decay;
}
